plugins {
    kotlin("jvm")
}

group = "vanadis.common.annotation"
version = "1.0.0"

dependencies {

    implementation(kotlin("stdlib-jdk8"))

    // Use the Kotlin test library.
//    testImplementation(kotlin("kotlin-test"))

    // Use the Kotlin JUnit integration.
//    testImplementation(kotlin("kotlin-test-junit"))
}